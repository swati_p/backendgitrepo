/**
 * Upasana.js
 *
 * @description ::This model defines an api built using waterfall model which will have multiple function to connect with different model
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var fs = require("fs");


module.exports = {

    tableName:'upasana',
    attributes: {
	  	title:{
	  			type:'string',
	  			required:true
	  		},
	  		lyrics:{
	  			type:'string',
	  			required:true
	  		},
	  		audioName:{
	  			type:'string',
	  			required:true
	  		},
        isFamous:{
            type:'boolean',
            defaultsTo:false
        },
        meaning:{
            type:'string'   
        }
  	},
	
    //////////////////////common api for all functions///////////////////////////////////////////
    getAppDetails:function(next){
    	console.log("inside model");
        var imagefolder='/images';
        var upasanaObj;
        var success = {
                    "status":false,
                    "message":null,                   
                    "dailyDarshanObj":null,
                    "upasanaObj":null,
                    "upasanaFamousObj":null,
                    "upcomingEventObj":null,
                    "allPhotoObj":null,
                    "videoObj":null,
                    "wallPaperObj":null,
                    "historyObj":null,
                    "bankObj":null,
                    "contactObj":null,
                    "dailyScheduleObj":null
        };

        async.waterfall([

            ////////////////////////function to getAllUpasana from upasana model///////////////////////////////////
            
            function getAllUpasana(callback){
            	dir='./.tmp/public/AudioFiles';
               Upasana.find().exec(function f(err,gUpasanaObj){               
                    if(err || !gUpasanaObj){
                      success.message=success.message+"Error occured in find function of upasanaObject";
                      callback(null,"dummyText");
                    }else if(gUpasanaObj){
                    	if(gUpasanaObj.length){
                        fs.readdir(dir,function(err,files){
                            if(err){
                             	success.message=success.message+"Error occured in readdir of upasanaObj";
                            }else{
                                async.each(gUpasanaObj,function(singleUpasanaObj,upasanaCB){
                                	files.forEach(function(audioFiles){
                                		var fileName = audioFiles.substr(0, audioFiles.lastIndexOf('.'));
                                		if(singleUpasanaObj.audioName==fileName){
					                						var audioName=audioFiles;
								                    	var filePath='/AudioFiles/'+audioName;
								                    	var audioPath=filePath.replace(/ /g,"%20");
								                    	singleUpasanaObj.audioPath=audioPath;
                  									}
                                	});
	                                upasanaCB();  
                                },function(err,result){
                                    if(err){
                                      success.message=success.message+"Error occured in callback of getAllUpasana";
                                    }
                                    else{
                                    	upasanaObj=gUpasanaObj;
                                      success.upasanaObj=gUpasanaObj;
                                      callback(null,upasanaObj);

                                    }
                                });
                            }
                        });
                      }
                    }
                });
            },

             ////////////////////////function to get famous upasana from Upasana Model///////////////////////////////////
            function getFamousUpasana(upasanaObj,callback){
            	var flag=false;
            	if(upasanaObj){
            		if(upasanaObj.length){
            			upasanaObj.forEach(function(upasanaSingleObj){
            				if(upasanaSingleObj.isFamous){
	            			success.upasanaFamousObj=upasanaSingleObj;
	            				flag=true;
	                  }
            			});
            			if(!flag){
	            		success.upasanaFamousObj=upasanaObj[0];
	            		}
	            	callback(null,"DummyText");
	            	}
            	}else{
            		success.message=success.message+"UpasanaObj not found!!!!";	
            		callback(null,"DummyText");
            	}
            },

            ////////////////////////function to get all upcomingEvents from Event Model///////////////////////////////////

            function getAllUpcomingEvents(next,callback){
            	
            	var fileArray=[];
                Event.find().exec(function f(err,upcomingEventsObj){               
                    if(err){
                      success.message=success.message+"Error occured in upcomingEvent";
                      callback(null,"DummyText");
                    }else if(upcomingEventsObj){
                        dir='./.tmp/public/images/UpcomingEventPhoto';
                        fs.readdir(dir,function(err,files){
                            if(err){
                             	success.message=success.message+"Error occured in readdir of getAllUpcomingEvents";
                            }else{
                                async.each(upcomingEventsObj,function(eventObj,eventsCallBack){
                                	files.forEach(function(fileWithExt){
                                		var fileName=fileWithExt.split(".")[0];
                                		if(fileName==eventObj.eventPhotoName){
                                			var filePath=fileWithExt;
                                			eventImagePath=imagefolder+'/UpcomingEventPhoto/'+filePath;
                                            var eventPath=eventImagePath.replace(/ /g,"%20");
                                			eventObj.imagePath=eventPath;
                                		}
                                	});
	                                eventsCallBack();  
                                },function(err,result){
                                    if(err){
                                       success.message=success.message+"Error occured in callback of upcominEvent";
                                    }
                                    else{
                                        success.upcomingEventObj=upcomingEventsObj;
                                        callback(null,"DummyText");
                                    }
                                });
                            }
                        });
                    }else{
                    	success.message=success.message+"No eventObj  found";
                    	callback(null,"dummyText");
                    }
                });
            },

             ////////////////////////function to get all photos sorted in descending order by latest event held///////////////////////////////////

            function getAllPhotos(next,callback){
            		var folderInfo=[];
                dir='./.tmp/public/images/photos';
                
                fs.readdir(dir,function(err,eventFolders){
                    if(err){        
                    		success.message=success.message+"Error occured in mainreadDir of getAllPhoto";
                    		callback(null,"DummyText");
                    }else{                             
                        async.each(eventFolders,function(eventFolderName,photoCB){
	                        var newDir=dir+'/'+eventFolderName;  
	                        var folderName=eventFolderName;                                   
	                        fs.stat(newDir,function(err,stats){
                            if(err){
                                success.message=success.message+"Error occured in statfile of getAllPhoto";
                            }else{                                            
                                var createdAt=stats.birthtime; 
                               	fs.readdir(newDir,function(err,file){
            										 	if(err){
            										 		success.message=success.message+"Error occured in readDir of getAllPhoto";
            										 	}
            										 	else{ 
            										 		file.forEach(function(imageFile){
            										 			var fileName=imageFile;
            										 			var photoPath=imagefolder+'/photos/'+folderName+'/'+fileName;
            										 			var imageUrl=photoPath.replace(/ /g,"%20");
          										 				var photoObj={
	                                   		folderName:folderName,
	                                   		createdAt:createdAt,
	                                   		filePath:imageUrl
		                                  }; 
          										 			 folderInfo.push(photoObj);
            										 		});	
            										 			photoCB();
            										 	}
            									 });                                                                          
                            }
	                        });
                        },function(err,result){
                            if(err){
                            	success.message=success.message+"Error occured in getAllPhoto";
                            }else{                        
                              success.allPhotoObj=_.sortBy(folderInfo,'createdAt').reverse();
                              callback(null,"DummyText");
                            }
                         });
                    }
                });
            },
           
            ////////////////////////function to get all daily darshan photo sorted in descending order ///////////////////////////////////

            function getAllDailyDarshanPhoto(next,callback){

            	var photoArray=[];
            	var fileName=null;
                dir='./.tmp/public/images/dailyDarshanPhoto';
                fs.readdir(dir,function(err,files){
                    if(err){
                        success.message=success.message+"Error occured in readDirectory of dailyDarshanPhoto"; 
                        callback(null,"DummyText");                 
                    }else{
                        async.each(files,function(file,dailyDarshanCB){  
                            //fileName=file;
                            filePath=imagefolder+'/dailyDarshanPhoto/'+file;
                            var imagePath=filePath.replace(/ /g,"%20");
                            fs.stat(dir+'/'+file,function(err,stats){
                                if(err){
                                    success.message=success.message+"Error occured in stat file";                   
                                }else{
                                    photoArray.push({
                                    	"filePath":imagePath,
                                      "fileName":file,
                                      "createdAt":stats.birthtime
                                    });
                                } 
                                dailyDarshanCB();
                            });
                        },
                        function(err,result){
                            if(err){
                                success.message=success.message+"Error occured in dailyDarshanCB";                    
                            }
                            else{
                                success.dailyDarshanObj= _.sortBy(photoArray,'createdAt').reverse();
                                callback(null,"DummyText");
                            }
                        }
                        );

                    }
                                   
                });
            },

            ////////////////////////function to get entire details from history model///////////////////////////////////

            function getHistory(next,callback){
                History.find().exec(function f(err,historyObj){
                    if(err){
                        success.message=success.message+"Error occured in getHistory function";  
                    }else if(historyObj){
                       success.historyObj=historyObj;
                    }else{                   
                    	success.message=success.message+"History Object not found!!!";  
                    }
                     callback(null,"DummyText"); 
                });
            },

            ////////////////////////function to get entire bankdetails from BankDetail model///////////////////////////////////
             
            function getBankDetails(next,callback){
                BankDetail.find().exec(function f(err,bankObj){
                    if(err){
                      success.message=success.message+"Error occured in getBankDetails function"; 
                    }else if(bankObj){
                      success.bankObj=bankObj; 
                    }else{
                    	success.message=success.message+"Error occured in getBankDetails function";
                    }
                    callback(null,"DummyText");
                });
            },

            ////////////////////////function to get entire contactDetails from ContactDetail model///////////////////////////////////

            function getContactDetails(next,callback){
                ContactDetails.find().exec(function f(err,contactObj){
                    if(err){
                       success.message=success.message+"Error occured in getContactDetails function"; 
                    }else if(contactObj){
                       success.contactObj=contactObj;
                    }else{
                    	success.message=success.message+"ContactObject not found!!!";
                    }
                    callback(null,"DummyText");  
                });
            },

            ////////////////////////function to get allVideos from Video model///////////////////////////////////
            function getAllVideos(next,callback){
                Video.find().exec(function f(err,videoObj){
                    if(err){
                       success.message=success.message+"Error occured in getAllVideos function"; 
                    }else if(videoObj){
                       success.videoObj=videoObj;
                       //callback(null,"DummyText");  
                    }else{
                    	 success.message=success.message+"VideoObjectnot found!!!"; 
                    }
                    callback(null,"DummyText");
                });
            },

             ////////////////////////function to get DailySchedule from DailySchedule model///////////////////////////////////
            function getDailySchedule(next,callback){
                DailySchedule.find().exec(function f(err,dailyScheduleObj){
                    if(err){
                       success.message=success.message+"Error occured in getDailySchedule function"; 
                    }else if(dailyScheduleObj){
                       success.dailyScheduleObj=dailyScheduleObj;
                    }else{
                    	success.message=success.message+"DailySchedule object not found!!!"; 
                    }
                     callback(null,"DummyText");  
                });
            },

            ////////////////////////function to get all wallPapers from wallPaper folder///////////////////////////////////
            function getAllWallPaper(next,callback){
            	 	var wallPaperArray=[];
                dir='./.tmp/public/images/wallPaper';
                fs.readdir(dir,function(err,files){
                    if(err){
                    		callback(null,"DummyText");
                        success.message=success.message+"Error occured in readDirectory of getAllWallPaper";                   
                    }else{
                        async.each(files,function(wallPaperImage,wallPaperCB){  
                            var wallPaperImage=wallPaperImage;
                            var filePath=imagefolder+'/wallPaper/'+wallPaperImage;
                            var imagePath=filePath.replace(/ /g,"%20");
                            wallPaperArray.push({
                            	imagePath:imagePath
                            });
                            wallPaperCB();
                        },
                        function(err,result){
                            if(err){
                                success.message=success.message+"Error occured in wallPaperCB";                    
                            }
                            else{
                                success.wallPaperObj= wallPaperArray;
                                callback(null,"DummyText");
                            }
                        }
                        );
                    }             
                });
            },

        ],function(err,waterfallCB){
            if(err){
                success.message=waterfallCB;
                next(success);
            }else{
                success.message="OK"+" " +success.message;
                success.status=true;              
                next(success);
            }
        });
    }
};

