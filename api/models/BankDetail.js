/**
 * Bankdetail.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName:'BankDetail',
  attributes: {
  	accountName:{
  		type:'string',
      required:true
  	},
  	accountNo:{
  		type:'integer',
      required:true
  	},
  	bankName:{
  		type:'string',
      required:true
  	},
  	ifscCodeNo:{
  		type:'string',
      required:true
  	}
  }
};

