/**
 * Event.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	tableName:'Event',
  attributes: {
  	title:{
  		type:'string',
      required:true
  	},
  	date:{
  		type:'string',
      required:true
  	},
  	time:{
  		type:'string',
      required:true
  	},
  	venue:{
  		type:'string',
      required:true
  	}

  }
};

