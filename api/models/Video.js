/**
 * Video.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName:'Video',
  attributes: {
  	eventTitle:{
  		type:'string',
      required:true
  	},
  	eventDate:{
  		type:'string',
      required:true
  	},
  	url:{
  		type:'string',
      required:true
  	}
  }
};

